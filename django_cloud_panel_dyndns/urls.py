from django.urls import path

from . import views

urlpatterns = [
    path("hostnames", views.list_hostnames),
    path("hostnames/create", views.modal_hostname_create),
    path("hostname/<str:hostname>", views.edit_hostname),
    path("hostname/<str:hostname>/delete", views.modal_hostname_delete),
]
