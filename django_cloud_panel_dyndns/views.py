from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.template import loader
from django.utils.translation import gettext as _

from django_cloud_panel_main.django_cloud_panel_main.views import get_nav
from django_session_ldap_attributes.django_session_ldap_attributes.decorators import (
    admin_group_required,
)


@admin_group_required("DNS")
def list_hostnames(request):
    template = loader.get_template("django_cloud_panel_dyndns/list_hostnames.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Internet Access")
    template_opts["content_title_sub"] = _("List Hostnames")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DNS")
def edit_hostname(request, hostname):
    template = loader.get_template("django_cloud_panel_dyndns/edit_hostname.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Internet Access")
    template_opts["content_title_sub"] = _("Edit Hostname") + " " + hostname

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DNS")
def modal_hostname_create(request):
    template = loader.get_template(
        "django_cloud_panel_dyndns/modal_hostname_create.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Internet Access")
    template_opts["content_title_sub"] = _("Create Hostname")

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("DNS")
def modal_hostname_delete(request, hostname):
    template = loader.get_template(
        "django_cloud_panel_dyndns/modal_hostname_delete.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Internet Access")
    template_opts["content_title_sub"] = _("Delete Hostname") + " " + hostname

    template_opts["hostname"] = hostname

    return HttpResponse(template.render(template_opts, request))
